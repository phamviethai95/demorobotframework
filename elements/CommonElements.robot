*** Variables ***
${MessagerLoginFail}            xpath=//android.widget.TextView[@text='Username and password do not match any user in this service.']
${MessagerUsernamRequired}            xpath=//android.widget.TextView[@text='Username is required']
${MessagerPasswordRequired}            xpath=//android.widget.TextView[@text='Password is required']
