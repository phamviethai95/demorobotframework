*** Settings ***
Resource            ../../imports.robot

*** Keywords ***
Verify Login fail : Username and password do not match any user in this service.
    wait until element is visible   ${MessagerLoginFail}   10s
    Page Should Contain Element         ${MessagerLoginFail}

Verify Login fail : Username is required.
    wait until element is visible   ${MessagerUsernamRequired}    10s
    Page Should Contain Element         ${MessagerUsernamRequired}

Verify Login fail : Password is required.
    wait until element is visible   ${MessagerPasswordRequired}   10s
    Page Should Contain Element         ${MessagerPasswordRequired}

Login With valid acc
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["validAcc"]["username"]}
    ${Password}     Set Variable    ${data["validAcc"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}


Login With Invalid Username
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["invalidUsername"]["username"]}
    ${Password}     Set Variable    ${data["invalidUsername"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Username and password do not match any user in this service.

Login With Invalid Password
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["invalidPassword"]["username"]}
    ${Password}     Set Variable    ${data["invalidPassword"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Username and password do not match any user in this service.

Login With Invalid Username And Password
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["invalidUserAndPassword"]["username"]}
    ${Password}     Set Variable    ${data["invalidUserAndPassword"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Username and password do not match any user in this service.

Login With empty Username
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["emptyUsername"]["username"]}
    ${Password}     Set Variable    ${data["emptyUsername"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Username is required.

Login With empty Password
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["emptyPassword"]["username"]}
    ${Password}     Set Variable    ${data["emptyPassword"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Password is required.

Login With empty Username And Password
    ${jsonfile}    Get File    ./Resource/dataUser.json
    ${data}    Evaluate    ${jsonfile}
    ${Username}     Set Variable    ${data["emptyUserAndPassword"]["username"]}
    ${Password}     Set Variable    ${data["emptyUserAndPassword"]["password"]}

    #input username and password.....
    wait until element is visible   ${userIdField}   10s
    input text   ${userIdField}      ${EMPTY}
    input text      ${userIdField}   ${Username}
    input text  ${passwordField}    ${EMPTY}
    input password   ${passwordField}    ${Password}

    #click on login......
    click element    ${btnLogin}
    Verify Login fail : Username is required.



