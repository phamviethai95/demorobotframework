*** Settings ***
# library
#Library           Selenium2Library
Library           AppiumLibrary
Library           String
Library           Collections
Library           OperatingSystem
Library           json

# elements
Resource          ./elements/LoginPage.robot
Resource          ./elements/CommonElements.robot


# keywords
Resource          ./keywords/common/CommonUtils.robot
Resource          ./keywords/page/LoginPages.robot


# config
#Variables         ./config_${env}.yaml

#variable
Resource          ./variables/common.robot
Resource          ./variables/login.robot
#Resource         ./variables/setup_device.robot
