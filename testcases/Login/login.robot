*** Settings ***
Resource            ../../imports.robot

Suite Setup      run keywords    Open App

*** Test Cases ***
tc1
    Login With Invalid Username
tc2
    Login With Invalid Password
tc3
    Login With Invalid Username And Password
tc4
    Login With empty Username
tc5
    Login With empty Password
tc6
    Login With empty Username And Password
tc7
    Login With valid acc
